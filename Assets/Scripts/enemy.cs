﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour {
    private GameObject target;
    private killSwitch killSwitch;
    private Collider2D col;
    private bool killable = false;
    private float speed = .02f;

    // Use this for initialization
    void Start () {
        GameManager.instance.AddEnemyToList(this);
        col = GetComponent<Collider2D>();
        GameObject[] switches = GameObject.FindGameObjectsWithTag("KillSwitch");
        target = GameObject.FindGameObjectWithTag("Player");
        killSwitch = switches[Random.Range(0, switches.Length)].GetComponent<killSwitch>();
        GetComponent<SpriteRenderer>().color = new Color(killSwitch.color.r, killSwitch.color.g, killSwitch.color.b, 255);
    }

    private void FixedUpdate()
    {
        transform.position = new Vector2((transform.position.x - speed), transform.position.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == killSwitch.gameObject) killable = true;
        if (collision.gameObject == target) GameManager.instance.resetGame();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        killable = false;
    }

    public bool checkShot()
    {
        Debug.Log(killSwitch.color + " - " + killable);
        if (killable) { die(); return true; }
        else { return false; }
    }

    public void die()
    {
        GameManager.instance.updateScore(1);
        Destroy(gameObject);
    }

    
}

