﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameHUD : MonoBehaviour {

    public Text scoreLabel;
    public Text strikesLabel;
    public Text highScoreLabel;

    // Use this for initialization
    void Start()
    {
        updateScoreHUD();
        updateStrikesHUD();
    }

    public void updateScoreHUD()
    {
        scoreLabel.text = "Score: " + GameManager.instance.score;
        highScoreLabel.text = "High Score: " + GameManager.instance.highScore;
    }

    public void updateStrikesHUD()
    {
        string strikesOut = "";
        for(int i = 0; i <= GameManager.instance.maxStrikes-1; i++)  strikesOut += (i < GameManager.instance.strikes ? "X " : "O ");
        strikesLabel.text = "Strikes: " + strikesOut;
    }
}
