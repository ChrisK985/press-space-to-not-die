﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;
    private List<enemy> enemies;
    public GameObject[] spawnableEnemies;
    private Timer enemySpawnTimer;
    private float spawnDelay = 2.5f;
    private float beginningSpawnRate;
    public int score = 0;
    public int highScore = 0;
    public int strikes = 0;
    public int maxStrikes = 3;
    public gameHUD hud;

    void Awake()
    {
        beginningSpawnRate = spawnDelay;
        if (instance == null) { instance = this; }
        else if (instance != this) { instance.hud = FindObjectOfType<gameHUD>(); Destroy(gameObject); }
        hud = FindObjectOfType<gameHUD>();

        DontDestroyOnLoad(gameObject);
        enemies = new List<enemy>();
        StartCoroutine(spawnEnemy());
    }

    public void updateScore(int s)
    {
        score += s;
        if (score > highScore) highScore = score;
        hud.updateScoreHUD();
    }

    public void updateStrikes(int s)
    {
        strikes += s;
        if (strikes == maxStrikes) { resetGame(); }
        else { hud.updateStrikesHUD(); }
        
    }

    IEnumerator spawnEnemy()
    {
        while (true)
        {
            GameObject enemyChoice = spawnableEnemies[Random.Range(0, spawnableEnemies.Length)];
            Instantiate(enemyChoice, new Vector3(3.36f, 0f), Quaternion.identity);
            spawnDelay = beginningSpawnRate - (float)(Mathf.Log(score + 1) * .40); //Higher score = lower spawn delay
            spawnDelay -= Random.Range(0f, (float)(spawnDelay*.75)); //Bonus random time taken off of spawn delay

            yield return new WaitForSeconds(spawnDelay);
        }
        
    }

    public void AddEnemyToList(enemy script)
    {
        enemies.Add(script);
    }

    public void shotFired()
    {
        bool enemiesHit = false;
        foreach(enemy e in enemies)
        {
            if(!enemiesHit) { enemiesHit = e.checkShot(); }
            else { e.checkShot(); }
        }
        if(!enemiesHit) GameManager.instance.updateStrikes(1);
    }

    public void resetGame()
    {
        score = 0;
        strikes = 0;
        enemies = new List<enemy>();
        SceneManager.LoadScene("Level1");
    }
}
