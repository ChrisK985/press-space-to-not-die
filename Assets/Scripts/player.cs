﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {

    bool isShooting = false;
	// Use this for initialization
	void Start () {
        isShooting = false;
    }
	
	// Update is called once per frame
	void Update () {
        shootHandler();
	}

    void shootHandler()
    {
        if (Input.GetKeyDown("space") && !isShooting)
        {
            GameManager.instance.shotFired();
            isShooting = true;
        }
        if(Input.GetKeyUp("space"))
        {
            isShooting = false;
        }
    }
}
